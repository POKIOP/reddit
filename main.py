from dataclasses import dataclass


import praw

CLIENT_ID = 'DK05OwgBu5dL5a_RaFDyCA'
CLIENT_SECRET = 'Fw864S6YWgHOCnfCPwvIt3MAw4PqcQ'
NUMBER_OF_POSTS = 20
ONE_NUM = 1
TITLE_INDEX_START = 0
TITLE_INDEX_STOP = 45
USER_AGENT = 'Tom App'


@dataclass
class Post:
    title: str
    score: int
    url: str

    def __str__(self) -> str:
        return f"{self.title[TITLE_INDEX_START:TITLE_INDEX_STOP]} | {self.score} | {self.url}"


def get_posts(reddit_authent, topic, number_of_posts):
    posts = []
    subreddit = reddit_authent.subreddit(topic)
    for post in subreddit.hot(limit=number_of_posts):
        posts.append(Post(post.title, post.score, post.url))
    return posts


def main():
    reddit_authent = praw.Reddit(client_id=CLIENT_ID, client_secret=CLIENT_SECRET, user_agent=USER_AGENT)
    topic = input("Enter searching topic: ")
    posts = get_posts(reddit_authent, topic, NUMBER_OF_POSTS)
    for count, post in enumerate(posts, start=ONE_NUM):
        print(f'{count:02d} | {post}')
    
    while True:
        post_number = int(input(f"\nEnter post number (from 1 to {NUMBER_OF_POSTS}): "))
        if post_number in range(ONE_NUM, NUMBER_OF_POSTS+ONE_NUM):
            print(f'\nPost about {topic} that you choose: {posts[post_number - ONE_NUM]}\n')
            break
        print("\nIncorrect post number, please try again.\n") 

if __name__ == "__main__":
    main()

